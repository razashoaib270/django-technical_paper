# Django
Django is a open source web framework and a high level language using python. Django follows the `MVC(Model View Controller)` architecture but in django it is known an `MVT(MOdel View Template)`

- **Model** - Django uses `ORM(Object Relational Mapping)` for definig data models of the database table from python classes and django automatically take care of creating, manipulating the database.

- **View** - View is the place where we write and save all our business logics, here we take request from the user and send response. This file interacts with the model to retrieve data and process data.

- **Template** - These are written in `DTL(Django Template Language)`: templating language which allows us to write python embeded HTML code which is displayed to the user.

## Settings.py
This file is located in the project root folder which  includescrucial configuration file that plays a central role in defining various settings and parameters for the project. 

- **Database configuration** : Here we chage our database preferences like which databse to use. default database provided by django is Sqlite.db

- **Debug mode** : The DEBUG setting is a boolean value that controls whether the application is in debug mode. In debug mode, detailed error pages are displayed, and certain development-related features are enabled.

- **Applications Installed** : Here we list all our applications we have installed manually, by default some appications are installed like the admin, auth, session and other which provide various features

- **Middleware** : Middleware is a way to process requests and responses globally before they reach the view or after they leave the view.This tag is responsible for the security, we can add our own securoties as well.

- **Static Files** : Settings like STATIC_URL, STATIC_ROOT, MEDIA_URL, and MEDIA_ROOT are used to configure the handling of static files (CSS, JavaScript, etc.) and media files (uploads).

- **Template** : This tag holds our template related setting and the path to the template

**NOTE**: Settings file should be handled carefully and you should not delete any setting.


## Views.py
This file include the business logic and all the request response stuff. It basically deals with the model file to fetch some data and pass that dat to the template file after doing some opertaion.

## Class Based View(CBV) and Function Based View(FBV)

**Function Based View**:
- It is a simple function that takes a request object as an argument and returns a response to the web browser.

    ```python
    from django.http import HttpResponse
    from django.shortcuts import render

    def index(request):
        # View logic goes here
        return HttpResponse("Hello, Function-Based View!")
    ```

**Class Based View**: 
- Class-based views are based on object-oriented principles, encapsulating both the HTTP methods and the view logic within class methods. Seperate view logic for different request type

    ```python
    from django.http import HttpResponse
    from django.views import View

    class MyView(View):
        def get(self, request):
            # View logic for GET request
            return HttpResponse("Hello, Class-Based View!")
    ```

## Urls.py

This is the file where we specify all our url/links to other views.
For our convenience wee create a urls.py in our app folder just to separate aur app specific urls from other apps but we have a urls.py by default in project folder.

To add url of a app we import include from django.urls
```python
# project level urls.py
from django.urls import path, include
urlpatterns = [
    path("", include('appname.urls')),
    path('admin/', admin.site.urls),
]

# app level urls.py
from django.urls import path
from . import views
app_name = "your_app_name"
urlpatterns = [
    path("", views.ViewName, name="view-name"),
    
    # URL patterns can include variables captured from the URL, allowing for dynamic and parameterized URLs.
    path('articles/<int:article_id>/', views.article_detail)
]
```


## models.py

Models.py is the file where we define classes for our database model( fields for our database). After defining the model we have to make migrations to make the change happen. Infact whenever we change our models.py we run the migrations command.
In models we have plenty of fields to use like `TextField()`, `CharField()` etc.....

```python
from django.db import models
class Article(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField()
    pub_date = models.DateTimeField('date published')
```

```shell
python3 manage.py makemigrations
python3 manage.py migrate
```

## ModelForms

Once we have our database table ready we can make a form out of the fields to take user inputs and store then in the database.
we just have to import the forms from django and rest will be handled by django.
```Python
from django import forms
from .models import Article

class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ['title', 'content', 'pub_date']
```

## Template
Django uses a template engine called Django Templates (DT). DT is used to generate HTML output dynamically.

Here also we have a project level(common to all the apps) and one app level(specific to a particular app component) template.
We have various template tags like:
- `{% if %}`
- `{% for %}`
- `{% endfor %}`
- `{% block %}`
- `{% endblock %}`
- `{% with %}`
- `{% endwith %}`
- `{% include %}`
- `{% load %}`
- `{% csrf_token %}`
- `{% static %}`
- `{% url %}` and many more........


## Static Files
Static files are those file in django project which include all our JavaScript, CSS and image assets. We load this in our template file using the `{% static %}` tag


