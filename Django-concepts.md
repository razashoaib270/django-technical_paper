## WSGI( Web Server Gateway Integration )
As The name suggest, it defines a simple interface for communication between web servers and our python application.

WSGI has two side:
- **`server/gateway side `** :  This is often running full web server software such as Apache or Nginx, or is a lightweight application server that can communicate with a webserver, such as flup.
- **`application/framework side`** : This is a Python callable, supplied by the Python program or framework.

In simple words it takes request from the user, hits the server, gets the response in the application side which is then modified as required and displayed. It is efficient way of fetting data as it doesnt puts load to our server.


## ORM queries

Object-Relational Mapping (ORM) is a programming concept that enables us to interact with databases using the object-oriented paradigm of any programming language. In the context of Django, the Django ORM provides a high-level, Pythonic way to interact with databases without having to write raw SQL queries.

- ORM in djando starts with definig the models which are nothing but a python class which defines the table of the database.

- The Django ORM uses QuerySets to perform database queries. A QuerySet is a collection of database queries that can be filtered, sliced, and generally manipulated.

    ```bash
    from appname.models import ModelName

    all_objects = MyModel.objects.all()
    filtered_objects = MyModel.objects.filter(field1='value1')
    single_object = MyModel.objects.get(pk=1)

    MyModel.objects.filter(field1='value1').delete()

    ```

## File Migrations

File Migrations includes of two step 
- `**makemigrations**`
    - This command checks for any update in models.py file by comparing it to the previoys migration and generates migration files that describe the changes to be made to the database schema.
    ```
    python manage.py makemigrations
    ```

- `**migrate**`
    - The migrate command apply those changes to the database. The migrate command reads the migration files and executes the SQL statements needed to update the database schema.
    ```
    python manage.py migrate
    ```


## Admin

admin.py file plays a key role in configuring the Django Admin interface for managing data in the Django admin site, it is also used to register the models

```python
# Example admin.py file

from django.contrib import admin
from .models import MyModel

class MyModelAdmin(admin.ModelAdmin):
    list_display = ('field1', 'field2', 'field3')  # Customize displayed fields
    search_fields = ('field1', 'field2')          # Add search functionality
    list_filter = ('field1', 'field2')  # Add filters

admin.site.register(MyModel, MyModelAdmin)
```

## XSS

`Cross-Site Scripting (XSS)` is a security vulnerability that occurs when an malicious scripts are injected in our application and includes untrusted data on a web page without proper validation or escaping.  XSS attacks occur when an attacker uses a web application to send malicious code, generally in the form of a browser side script, to a different end user. In the context of Django, preventing XSS attacks is crucial for maintaining the security of web applications.

```
{{ user_input|safe }}
{% autoescape on %}
{{ user_input }}
{% endautoescape %}
```

## CSRF( Cross-site Request Forgery )
- **`CSRF protection`** : Django protects against CSRF attacks by using a unique token( some random value ) included in forms as a hidden field or in the header of each HTTP request for each user session.

```html
<form method="post" action="{% url 'my_view' %}">
    {% csrf_token %}
    <!-- Other form fields go here -->
    <input type="submit" value="Submit">
</form>
```